import React from 'react';
import Inner from './Inner/Inner';
import Main from './Main/Main';
import { BrowserRouter, Route, Switch } from "react-router-dom";

function Content() {
    return (
        <div className="l-content">
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={Main} />
                    <Route path="/inner" component={Inner} />
                </Switch>
            </BrowserRouter>
        </div>
    )
}

export default Content
