import React from 'react'

function Header({htmlClass, title}) {
    return (
        <>
        <div className={"l-header" + htmlClass}>
            <h1>Шапка {title}</h1>
        </div>
        <div className="l-slider">
            Слайдер
        </div>
        </>
    )
}

Header.defaultProps = {htmlClass: ''}

export default Header
